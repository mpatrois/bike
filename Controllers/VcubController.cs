using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bikeMvc.Models;

namespace bikeMvc.Controllers{
    public class VcubController : Controller
    {
        [HttpGet("vcub/stations/liste")]
        public IActionResult Liste()
        {
            var model = new BikeStationModel();
            return View(model);
        }


        [HttpGet("vcub/stations/carte")]
        public IActionResult Carte()
        {
            var model = new BikeStationModel();
            return View(model);
        }

        [HttpGet("vcub/stations/{id}")]
        public IActionResult Detail(int id)
        {
            var model = new BikeStationModel();
            var station = model.getStation(id);
            return View(station);
        }

        [HttpGet("vcub/stations/{id}/signaler/{idVelo}")]
        public IActionResult Signaler(int id,int idVelo)
        {
            var model = new BikeStationModel();
            var station = model.getStation(id);
            var signalerBike = new SignalerBike(station, idVelo);
            return View(signalerBike);
        
        }

        [HttpPost("vcub/stations/{id}/signaler/{idVelo}")]
        public IActionResult Signaler(SignalerBike model)
        {   
            Console.WriteLine(model.commentaire);
            TempData["signaler"] = model.commentaire;
            return RedirectToAction("ThankYou");
        }

        [HttpGet("vcub/stations/thank-you")]
        public IActionResult ThankYou()
        {
            // SignalerBike signaler = (SignalerBike)TempData["signaler"];
            ViewBag.signalerMessage = TempData["signaler"];
            return View();
        }

    }
}