using System;
using Newtonsoft.Json;

namespace bikeMvc.Models
{
    public class SignalerBike
    {
        public SignalerBike()
        {
            this.station = null;
            this.IdVelo = -1;
            this.commentaire = "";
            this.roueDefail = false;
            this.pedDefail = false;
            this.freinDefail = false;
            this.selleDefail = false;
        }

        public SignalerBike(BikeStation station,int IdVelo)
        {
            this.station = station;
            this.IdVelo  = IdVelo;
            this.commentaire = "";
            this.roueDefail = false;
            this.pedDefail = false;
            this.freinDefail = false;
            this.selleDefail = false;
        }
        public BikeStation station { get; set; }

        public int IdVelo { get; set; }
        public string commentaire { get; set; }
        public bool roueDefail {get; set;}
        public bool pedDefail {get; set;}
        public bool freinDefail {get; set;}
        public bool selleDefail {get; set;}

    }
}