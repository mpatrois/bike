using System;

namespace bikeMvc.Models
{
    public class Rapper
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}