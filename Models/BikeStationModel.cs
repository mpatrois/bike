using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace bikeMvc.Models
{
    public class BikeStationModel
    {
        public List<BikeStation> StationList { get; set; }

        public BikeStationModel()
        {
            using (var client = new HttpClient()){
                var response = client.GetAsync("http://api.alexandredubois.com/vcub-backend/vcub.php");
                var stringResult = response.Result.Content.ReadAsStringAsync();
                StationList = JsonConvert.DeserializeObject<List<BikeStation>>(stringResult.Result);
            }
        }

        public BikeStation getStation(int id){
            foreach (var station in StationList)
            {
                if(station.Id == id){
                    return station;
                }
            }
            return null;
        }

        // public BikeStationModel()
        // {
        //     using (var client = new HttpClient())
        //     {
        //         var response = client.GetAsync("http://api.alexandredubois.com/vcub-backend/vcub.php");
        //         var stringResult = response.Result.Content.ReadAsStringAsync();
        //         StationList = JsonConvert.DeserializeObject<List<BikeStation>>(stringResult.Result);
        //     }
        // }
    }
}