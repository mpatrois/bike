using Microsoft.EntityFrameworkCore;

namespace bikeMvc.Models
{
    public class mvcRaperContext : DbContext
    {
        public mvcRaperContext (DbContextOptions<mvcRaperContext> options)
            : base(options)
        {
        }

        public DbSet<bikeMvc.Models.Rapper> Movie { get; set; }
    }
}