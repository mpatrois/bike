
using System;
using Newtonsoft.Json;

namespace bikeMvc.Models
{
    public class BikeStation
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")] 
        public int Id { get; set; }

        [JsonProperty("bike_count")]
        public int BikeCount { get; set; }

        [JsonProperty("slot_count")]
        public int SlotCount { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("is_online")]
        public bool IsAvailable { get; set; }
    }
}