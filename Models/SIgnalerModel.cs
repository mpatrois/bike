using System;
using Newtonsoft.Json;

namespace bikeMvc.Models
{
    public class SignalerModel
    {
        public SignalerModel(int idStation, int IdVelo, string message)
        {
            this.idStation = idStation;
            this.idVelo = IdVelo;
            this.message = message;
        }

        public int idStation { get; set; }
        public int idVelo { get; set; }
        public string message { get; set; }

    }
}